'use strict';

app.ticketing = kendo.observable({
    onShow: function() {},
    afterShow: function() {
        ble.isEnabled(onBLESuccess , onBLEFailure);
    },
    onBLESuccess: function(){
        
    },
    onBLEFailure: function(){
        alert("Please Enable your Bluetooth");
    }
});

var lineNumber;

// START_CUSTOM_CODE_ticketing
// END_CUSTOM_CODE_ticketing
(function(parent) {
    var dataProvider = app.data.defaultProvider,
        processImage = function(img) {
            if (!img) {
                var empty1x1png = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQI12NgYAAAAAMAASDVlMcAAAAASUVORK5CYII=';
                img = 'data:image/png;base64,' + empty1x1png;
            } else if (img.slice(0, 4) !== 'http' &&
                img.slice(0, 2) !== '//' &&
                img.slice(0, 5) !== 'data:') {
                var setup = dataProvider.setup;
                img = setup.scheme + ':' + setup.url + setup.apiKey + '/Files/' + img + '/Download';
            }

            return img;
        },
        flattenLocationProperties = function(dataItem) {
            var propName, propValue,
                isLocation = function(value) {
                    return propValue && typeof propValue === 'object' &&
                        propValue.longitude && propValue.latitude;
                };

            for (propName in dataItem) {
                if (dataItem.hasOwnProperty(propName)) {
                    propValue = dataItem[propName];
                    if (isLocation(propValue)) {
                        // Location type property
                        dataItem[propName] =
                            kendo.format('Latitude: {0}, Longitude: {1}',
                                propValue.latitude, propValue.longitude);
                    }
                }
            }
        },
        swipeView = function(){
            app.mobileApp.navigate('#components/home/view.html');
        },
        dataSourceOptions = {
            type: 'everlive',
            transport: {
                typeName: 'Travel',
                dataProvider: dataProvider
            },

            change: function(e) {
                var data = this.data();
                for (var i = 0; i < data.length; i++) {
                    var dataItem = data[i];
					dataItem['imageUrl'] =
                        processImage(dataItem['Image']);
                    flattenLocationProperties(dataItem);
                }
            },
            schema: {
                model: {
                    fields: {
                        'Title': {
                            field: 'Title',
                            defaultValue: ''
                        },
                        'Price': {
                            field: 'Price',
                            defaultValue: ''
                        },
                    },
                    icon: function() {
                        var i = 'globe';
                        return kendo.format('km-icon km-{0}', i);
                    }
                }
            },
            serverSorting: true,
            serverPaging: true,
            pageSize: 50
        },
        dataSource = new kendo.data.DataSource(dataSourceOptions),
        ticketingModel = kendo.observable({
            dataSource: dataSource,
            itemClick: function(e) {
                app.mobileApp.navigate('#components/ticketing/details.html?uid=' + e.dataItem.uid);
            },
            detailsShow: function(e) {
                var item = e.view.params.uid,
                    dataSource = ticketingModel.get('dataSource'),
                    itemModel = dataSource.getByUid(item);
                if (!itemModel.Title) {
                    itemModel.Title = String.fromCharCode(160);
                }
                ticketingModel.set('currentItem', itemModel);
            },
            busShow: function(e){
                var busNumber = e.view.params;
                lineNumber = e.view.params.busNumber;
                console.log(busNumber);
                ticketingModel.set('currentBus',busNumber);
            },
            ticketShow: function(e){
                var ticketDetail = e.view.params;
                ticketingModel.set('currentTicket' , ticketDetail);
                console.log(e.view.params);
            },
            currentTicket: null,
            currentBus: null,
            currentItem: null
        });

    parent.set('ticketingModel', ticketingModel);
})(app.ticketing);

// START_CUSTOM_CODE_ticketingModel
var estBeacon = (function(){
       
    var monitor = function(){
        startMonitoringBeacons();
    };
    
    var formatDistance = function(meters) {
		if (!meters) { return 'Unknown'; }

		if (meters > 1)
		{
			return meters.toFixed(3) + ' m';
		}
		else
		{
			return (meters * 100).toFixed(3) + ' cm';
		}
	};
    
    var proximityNames = [
		'unknown',
		'immediate',
		'near',
		'far'];

	var formatProximity = function(proximity) {
		if (!proximity) { return 'Unknown'; }

		// Eliminate bad values (just in case).
		proximity = Math.max(0, proximity);
		proximity = Math.min(3, proximity);

		// Return name for proximity.
		return that.proximityNames[proximity];
	};
    
    var startMonitoringBeacons = function (){
        
        function onRange(beaconInfo)
		{
			displayBeconInfo(beaconInfo);
		}

		function onError(errorMessage)
		{
			console.log('Range error: ' + errorMessage);
		}
        
        function displayBeconInfo(beaconInfo)
		{
			// Clear beacon HTML items.
			$('#id-screen-range-beacons .style-item-list').empty();

			// Sort beacons by distance.
			beaconInfo.beacons.sort(function(beacon1, beacon2) {
				return beacon1.distance > beacon2.distance; });

			// Generate HTML for beacons.
			$.each(beaconInfo.beacons, function(key, beacon)
			{
				var element = $(createBeaconHTML(beacon));
				$('#id-screen-range-beacons .style-item-list').append(element);
			});
		};

    
        var createBeaconHTML = function (beacon){
                var htm = '<div>'
                    + '<table><tr><td>Major</td><td>' + beacon.major
                    + '</td></tr><tr><td>Minor</td><td>' + beacon.minor
                    + '</td></tr><tr><td>RSSI</td><td>' + beacon.rssi;
                
                if (beacon.proximity)
                {
                    htm += '</td></tr><tr><td>Proximity</td><td>'
                        + formatProximity(beacon.proximity)
                }
                if (beacon.distance)
                {
                    htm += '</td></tr><tr><td>Distance</td><td>'
                        + formatDistance(beacon.distance)
                }
                htm += '</td></tr></table></div>';
                return htm;
        }
    
    	// Request authorisation.
		estimote.beacons.requestAlwaysAuthorization();

		// Start ranging.
		estimote.beacons.startRangingBeaconsInRegion({},onRange,onError);
        
        //start Ranging Beacons
    }
    
    var onBeaconsReceived = function (result) {
    if (result.beacons && result.beacons.length > 0) {
     
        var msg = "Busses Near You : " + result.beacons.length + "<br/>";
        
        var busCount = "Busses Near You : " + result.beacons.length + "<br/>";
        // var arr = [];
        var newDiv =  "Busses Near You : " + result.beacons.length + "<br/>"; 
        for (var i=0; i<result.beacons.length; i++) {
            var beacon = result.beacons[i];
            
            var img_placeholder, busDiv, bus_number, distance , arrow; 
            img_placeholder = "<img class='busIcon' src='images/blue_bus.png' width='40px' height='auto'/>";
               
            /*if (beacon.color !== undefined) {
                 msg += "Color: " + beacon.color + "<br/>";
            }
            if (beacon.macAddress !== undefined) {
                msg += "Mac Address: " + beacon.macAddress + "<br/>";
            }*/
            
            if(beacon.major == 19308){
                bus_number = "109";
            }else if(beacon.major == 54527){
                bus_number = "207";
            }
            
            distance = "<span class='busTextMiles'>" + beacon.distance.toFixed(2) + " meters</span>";
            
            busDiv = "<div class='busText'><p>"+bus_number+"</p>"+distance+"</div>";
            
            arrow = "<img class='arrow' src='images/arrow.png'/>";
            
            newDiv += "<div class='buslist_item' data-bus-number=' " + bus_number + "'>" + img_placeholder+ busDiv + arrow+"</div>";
           
        }
        document.getElementById('beaconlog').innerHTML = newDiv;
    }
}

// wiring the fired event of the plugin to the callback function
    
    return {
        monitor:monitor,
        onBeaconsReceived:onBeaconsReceived
    };
    
})(); 

document.addEventListener("deviceready",function(e){
	window.estimote.startRanging("Telerik");  
   	document.addEventListener('beaconsReceived', estBeacon.onBeaconsReceived, false);
});
 
$(document).on("click", ".buslist_item",function() {
    var num = $(this).data("bus-number");
    localStorage.setItem("busNumber",num);
    app.mobileApp.navigate('#components/ticketing/destination.html?busNumber=' + num);
});

$(document).on("click","#loader",function() {
    app.mobileApp.navigate('#components/ticketing/destination.html?busNumber=' + "109");
})

$(document).on("click",".payButton" , function(){
    var lineNum = $(this).data("url");
    var destVal = $('.destSelect').val();
    var tickeType = $('input[name=radioTicket]:checked','.ticketType').val();
    console.log("Selcted Item "+destVal);
    console.log("Selected Ticket Type "+tickeType);
    if(destVal == 0){
        alert("Please select the destination");
    }else{
        var selected= $('.destSelect').find(":selected").text();
        localStorage.setItem("destination", selected);
        localStorage.setItem("type" , tickeType);
        console.log(selected);
        app.mobileApp.navigate('#components/ticketing/ticket.html?busNumber=line ' + lineNumber +'&destination='+selected+'&type='+tickeType);
    }
});

var dt = new Date();
var min;
if(dt.getMinutes() < 10){
  min = "0"+dt.getMinutes();  
}else{
    min = dt.getMinutes();
}
var time = dt.getHours() + ":" + min;
var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
var currentdate = dt.getDate()+ "th " + monthNames[dt.getMonth()] +" "+ dt.getFullYear();




//estBeacon.monitor();
// END_CUSTOM_CODE_ticketingModel

