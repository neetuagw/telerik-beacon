'use strict';

app.home = kendo.observable({
    onShow: function() {},
    afterShow: function() {}
});

// START_CUSTOM_CODE_home
// END_CUSTOM_CODE_home
(function(parent) {
    var dataProvider = app.data.defaultProvider,
        processImage = function(img) {
            if (!img) {
                var empty1x1png = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQI12NgYAAAAAMAASDVlMcAAAAASUVORK5CYII=';
                img = 'data:image/png;base64,' + empty1x1png;
            } else if (img.slice(0, 4) !== 'http' &&
                img.slice(0, 2) !== '//' &&
                img.slice(0, 5) !== 'data:') {
                var setup = dataProvider.setup;
                img = setup.scheme + ':' + setup.url + setup.apiKey + '/Files/' + img + '/Download';
            }

            return img;
        },
        flattenLocationProperties = function(dataItem) {
            var propName, propValue,
                isLocation = function(value) {
                    return propValue && typeof propValue === 'object' &&
                        propValue.longitude && propValue.latitude;
                };

            for (propName in dataItem) {
                if (dataItem.hasOwnProperty(propName)) {
                    propValue = dataItem[propName];
                    if (isLocation(propValue)) {
                        // Location type property
                        dataItem[propName] =
                            kendo.format('Latitude: {0}, Longitude: {1}',
                                propValue.latitude, propValue.longitude);
                    }
                }
            }
        },
        dataSourceOptions = {
            type: 'everlive',
            transport: {
                typeName: 'Deals',
                dataProvider: dataProvider
            },

            change: function(e) {
                var data = this.data();
                for (var i = 0; i < data.length; i++) {
                    var dataItem = data[i];

                    dataItem['imageUrl'] =
                        processImage(dataItem['image']);

                    flattenLocationProperties(dataItem);
                }
            },
            schema: {
                model: {
                    fields: {
                        'Title': {
                            field: 'Title',
                            defaultValue: ''
                        },
                        'Summary': {
                            field: 'Summary',
                            defaultValue: ''
                        },
                        'image': {
                            field: 'image',
                            defaultValue: ''
                        },
                    }
                }
            },
        },
        dataSource = new kendo.data.DataSource(dataSourceOptions),
        homeModel = kendo.observable({
            dataSource: dataSource,
            itemClick: function(e) {
                app.mobileApp.navigate('#components/home/details.html?uid=' + e.dataItem.uid);
            },
            payClick:function(e){
                
                console.log(e);
                 app.mobileApp.navigate('#components/home/Receipt.html?uid=' + e.dataItem.uid);
            },
            detailsShow: function(e) {
                var item = e.view.params.uid,
                    dataSource = homeModel.get('dataSource'),
                    itemModel = dataSource.getByUid(item);
                itemModel.imageUrl = processImage(itemModel.image);
                if (!itemModel.Title) {
                    itemModel.Title = String.fromCharCode(160);
                }
                homeModel.set('currentItem', itemModel);
            },
            currentItem: null
        });

    parent.set('homeModel', homeModel);
})(app.home);

$(document).on("click",".payDeal" , function(e){
// app.homeModel.payClick();
app.mobileApp.navigate('#components/home/listOne.html');
});

var scanBeacon = (function(){
    var onBeaconsReceived = function (result) {
        if (result.beacons && result.beacons.length > 0) {
            
            cordova.plugins.backgroundMode.setDefaults({
                title:  'Ready for your Ride',
                ticker: 'This is sticker',
                text:   'You have ' + result.beacons.length+' Buses near you'
            });
            for (var i=0; i<result.beacons.length; i++) {
                var beacon = result.beacons[i];
            }

        }
	}
    
    return {
        onBeaconsReceived:onBeaconsReceived
    };
    
})();

document.addEventListener('deviceready',function(){
    // Android customization
    cordova.plugins.backgroundMode.setDefaults({ text:'Looking for Beacons.'});
    // Enable background mode
    cordova.plugins.backgroundMode.enable();
	// Called when background mode has been activated
    cordova.plugins.backgroundMode.onactivate = function () {
        window.estimote.startRanging("BackgroundScan");  
   		document.addEventListener('beaconsReceived', scanBeacon.onBeaconsReceived, false);
    };
    
    cordova.plugins.backgroundMode.onfailure = function(errorCode) {
        alert('BackgroundMode cant be enabled' + errorCode);
    };
    
    cordova.plugins.backgroundMode.ondeactivate = function() {
        //alert('Background Mode deacivated');
    };
}, false);

// START_CUSTOM_CODE_homeModel
// END_CUSTOM_CODE_homeModel