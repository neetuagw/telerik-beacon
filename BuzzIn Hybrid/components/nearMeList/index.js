'use strict';

app.nearMeList = kendo.observable({
    onShow: function() {},
    afterShow: function() {
        ble.isEnabled(onBLESuccess , onBLEFailure);
    },
    onBLESuccess: function(){
        
    },
    onBLEFailure: function(){
        //ble.showBluetoothSettings(onSuccess, onFailure);
        alert("Please Enable your Bluetooth");
        
    }
});

// START_CUSTOM_CODE_nearMeList
// END_CUSTOM_CODE_nearMeList
(function(parent) {
    var dataProvider = app.data.defaultProvider,
        processImage = function(img) {
            if (!img) {
                var empty1x1png = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQI12NgYAAAAAMAASDVlMcAAAAASUVORK5CYII=';
                img = 'data:image/png;base64,' + empty1x1png;
            } else if (img.slice(0, 4) !== 'http' &&
                img.slice(0, 2) !== '//' &&
                img.slice(0, 5) !== 'data:') {
                var setup = dataProvider.setup;
                img = setup.scheme + ':' + setup.url + setup.apiKey + '/Files/' + img + '/Download';
            }

            return img;
        },
        flattenLocationProperties = function(dataItem) {
            var propName, propValue,
                isLocation = function(value) {
                    return propValue && typeof propValue === 'object' &&
                        propValue.longitude && propValue.latitude;
                };

            for (propName in dataItem) {
                if (dataItem.hasOwnProperty(propName)) {
                    propValue = dataItem[propName];
                    if (isLocation(propValue)) {
                        // Location type property
                        dataItem[propName] =
                            kendo.format('Latitude: {0}, Longitude: {1}',
                                propValue.latitude, propValue.longitude);
                    }
                }
            }
        },
        dataSourceOptions = {
            type: 'everlive',
            transport: {
                typeName: 'Deals',
                dataProvider: dataProvider
            },

            change: function(e) {
                var data = this.data();
                for (var i = 0; i < data.length; i++) {
                    var dataItem = data[i];

                    dataItem['imageUrl'] =
                        processImage(dataItem['image']);

                    flattenLocationProperties(dataItem);
                }
            },
            schema: {
                model: {
                    fields: {
                        'Title': {
                            field: 'Title',
                            defaultValue: ''
                        },
                        'Summary': {
                            field: 'Summary',
                            defaultValue: ''
                        },
                        'image': {
                            field: 'image',
                            defaultValue: ''
                        },
                    }
                }
            },
        },
        dataSource = new kendo.data.DataSource(dataSourceOptions),
        nearMeListModel = kendo.observable({
            dataSource: dataSource,
            itemClick: function(e) {
                app.mobileApp.navigate('#components/nearMeList/details.html?uid=' + e.dataItem.uid);
            },
            detailsShow: function(e) {
                var item = e.view.params.uid,
                    dataSource = nearMeListModel.get('dataSource'),
                    itemModel = dataSource.getByUid(item);
                itemModel.imageUrl = processImage(itemModel.image);
                if (!itemModel.Title) {
                    itemModel.Title = String.fromCharCode(160);
                }
                nearMeListModel.set('currentItem', itemModel);
            },
            offerShow : function(e){
                var offerName = e.view.params;
                nearMeListModel.set('currentOffer' , offerName);
                console.log(e.view.params.offerNum);
            },
            currentOffer: null,
            currentItem: null
        });

    parent.set('nearMeListModel', nearMeListModel);
})(app.nearMeList);

var offerEstBeacon = (function(){
       
    var monitor = function(){
        startMonitoringBeacons();
    };
    
    var formatDistance = function(meters) {
		if (!meters) { return 'Unknown'; }

		if (meters > 1)
		{
			return meters.toFixed(3) + ' m';
		}
		else
		{
			return (meters * 100).toFixed(3) + ' cm';
		}
	};
    
    var proximityNames = [
		'unknown',
		'immediate',
		'near',
		'far'];

	var formatProximity = function(proximity) {
		if (!proximity) { return 'Unknown'; }

		// Eliminate bad values (just in case).
		proximity = Math.max(0, proximity);
		proximity = Math.min(3, proximity);

		// Return name for proximity.
		return that.proximityNames[proximity];
	};
    
    var startMonitoringBeacons = function (){
        
        function onRange(beaconInfo)
		{
			displayBeconInfo(beaconInfo);
		}

		function onError(errorMessage)
		{
			console.log('Range error: ' + errorMessage);
		}
        
        function displayBeconInfo(beaconInfo)
		{
			// Clear beacon HTML items.
			$('#id-screen-range-beacons .style-item-list').empty();

			// Sort beacons by distance.
			beaconInfo.beacons.sort(function(beacon1, beacon2) {
				return beacon1.distance > beacon2.distance; });

			// Generate HTML for beacons.
			$.each(beaconInfo.beacons, function(key, beacon)
			{
				var element = $(createBeaconHTML(beacon));
				$('#id-screen-range-beacons .style-item-list').append(element);
			});
		};

    
        var createBeaconHTML = function (beacon){
                var htm = '<div>'
                    + '<table><tr><td>Major</td><td>' + beacon.major
                    + '</td></tr><tr><td>Minor</td><td>' + beacon.minor
                    + '</td></tr><tr><td>RSSI</td><td>' + beacon.rssi;
                
                if (beacon.proximity)
                {
                    htm += '</td></tr><tr><td>Proximity</td><td>'
                        + formatProximity(beacon.proximity)
                }
                if (beacon.distance)
                {
                    htm += '</td></tr><tr><td>Distance</td><td>'
                        + formatDistance(beacon.distance)
                }
                htm += '</td></tr></table></div>';
                return htm;
        }
    
    	// Request authorisation.
		estimote.beacons.requestAlwaysAuthorization();

		// Start ranging.
		estimote.beacons.startRangingBeaconsInRegion({},onRange,onError);
        
        //start Ranging Beacons
    }
    
    var onBeaconsReceived = function (result) {
    if (result.beacons && result.beacons.length > 0) {
        
        // if (cordova.plugins){
        //      cordova.plugins.notification.local.schedule({
        //       id         : 1,
        //       title      : 'Buses Near you',
        //       text       : 'We found Busses near you'+result.beacons.length,
        //       autoClear  : true,
        //       at         : this.getNowPlus10Seconds()
        //     });
        // }
        
        var msg = "Busses Near You : " + result.beacons.length + "<br/>";
        
        var busCount = "Busses Near You : " + result.beacons.length + "<br/>";
        // var arr = [];
        var offerDiv =  "Providers Near You : " + result.beacons.length + "<br/>"; 
        for (var i=0; i<result.beacons.length; i++) {
            var beacon = result.beacons[i];
            
            var img_placeholder, busDiv, offer_num, distance , arrow; 
            img_placeholder = "<img class='busIcon' src='images/blue_bus.png' width='40px' height='auto'/>";
               
            /*if (beacon.color !== undefined) {
                 msg += "Color: " + beacon.color + "<br/>";
            }
            if (beacon.macAddress !== undefined) {
                msg += "Mac Address: " + beacon.macAddress + "<br/>";
            }*/
            
            if(beacon.major == 19308){
                offer_num = "Marks and Spencers";
            }else if(beacon.major == 54527){
                offer_num = "Mc Donald";
            }
            
            distance = "<span class='busTextMiles'>" + beacon.distance.toFixed(2) + " meters</span>";
            
            busDiv = "<div class='busText'><p>"+offer_num+"</p>"+distance+"</div>";
            
            arrow = "<img class='arrow' src='images/arrow.png'/>";
            
            offerDiv += "<div class='offerlist_item' data-offer-number=' " + offer_num + "'>" + img_placeholder+ busDiv + arrow+"</div>";
           
        }
        document.getElementById('restLog').innerHTML = offerDiv;
        
        // setInterval(function(){
        //     document.getElementById('beaconlog').innerHTML = offerDiv;
        // } , 15000);
        
    }
}

// wiring the fired event of the plugin to the callback function
    
    return {
        monitor:monitor,
        onBeaconsReceived:onBeaconsReceived
    };
    
})(); 

function onBLESuccess(){
    //alert("Bluetooth is enabled already");
}

function onBLEFailure(){
    alert("Please Enable your Bluetooth");
}

document.addEventListener("deviceready",function(e){
	window.estimote.startRanging("Offers");  
   	document.addEventListener('beaconsReceived', offerEstBeacon.onBeaconsReceived, false);
    // document.addEventListener('beaconsReceived', function(){
    //     setInterval(offerEstBeacon.onBeaconsReceived , 15000);
    // });
});

$(document).on("click", ".offerlist_item",function() {
    var name = $(this).data("offer-number");
    localStorage.setItem("offerNumber",name);
    app.mobileApp.navigate('#components/nearMeList/offers.html?offerNum=' + name);
});

$(document).on("click","#offerLoader",function() {
    app.mobileApp.navigate('#components/nearMeList/offers.html?offerNum=' + "Marks and Spensers");
});


// START_CUSTOM_CODE_nearMeListModel
// END_CUSTOM_CODE_nearMeListModel